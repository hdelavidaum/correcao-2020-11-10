import { useState, useEffect } from "react";
import "./App.css";

const App = () => {
  const [characterAPI, setCharacterAPI] = useState({
    characterList: [],
    nextUrl: "https://rickandmortyapi.com/api/character/",
  });

  const { characterList, nextUrl } = characterAPI;

  const getCharacters = () => {
    if (nextUrl) {
      fetch(nextUrl)
        .then((res) => res.json())
        .then((body) => {
          setCharacterAPI({
            characterList: [...characterList, ...body.results],
            nextUrl: body.info.next, // se não houver próxima página, o next será null
          });
        });
    }
  };

  useEffect(getCharacters, [nextUrl, characterList]);

  return (
    <div className="App">
      <header className="App-header">
        <div>{characterList.length}</div>
        {characterList.map((character, index) => (
          <div>{character.name}</div>
        ))}
      </header>
    </div>
  );
};

export default App;
